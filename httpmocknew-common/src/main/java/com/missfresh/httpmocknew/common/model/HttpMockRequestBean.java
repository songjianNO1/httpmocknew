package com.missfresh.httpmocknew.common.model;

import java.util.Date;

public class HttpMockRequestBean extends Page {
    private static final long serialVersionUID = 1L;

    private  Long id;
    //    @JSONField(format = "yyyy-MM-dd")
    private Date create_time;

    private String service_name;
    private String method_type;
    private String request_path;
    private String request_query;
    private String request_body;
    private String response_body;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getMethod_type() {
        return method_type;
    }

    public void setMethod_type(String method_type) {
        this.method_type = method_type;
    }

    public String getRequest_path() {
        return request_path;
    }

    public void setRequest_path(String request_path) {
        this.request_path = request_path;
    }

    public String getRequest_query() {
        return request_query;
    }

    public void setRequest_query(String request_query) {
        this.request_query = request_query;
    }

    public String getRequest_body() {
        return request_body;
    }

    public void setRequest_body(String request_body) {
        this.request_body = request_body;
    }

    public String getResponse_body() {
        return response_body;
    }

    public void setResponse_body(String response_body) {
        this.response_body = response_body;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
