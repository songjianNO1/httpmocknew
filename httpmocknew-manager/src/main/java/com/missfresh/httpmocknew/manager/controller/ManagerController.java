package com.missfresh.httpmocknew.manager.controller;

import com.missfresh.httpmocknew.common.model.HttpMock;
import com.missfresh.httpmocknew.common.model.HttpMockRequestBean;
import com.missfresh.httpmocknew.common.model.Result;
import com.missfresh.httpmocknew.common.utils.JsonUtil;
import com.missfresh.httpmocknew.service.IHttpMockService;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@EnableAutoConfiguration
@RequestMapping(value="/httpmock")
public class ManagerController {
//    private static final Logger logger = Logger.getLogger(ManagerController.class);

    @Autowired
    IHttpMockService httpmockService;


    @ResponseBody
    @RequestMapping(value="/queryAll",method ={RequestMethod.GET})
    //http://localhost:18099/queryAll?currentPageNum=1&pageSize=20
    public Result mockGet(HttpMockRequestBean httpMockRequestBean){
//        logger.info("请求数据为："+JsonUtil.object2string(httpMockRequestBean));

        List<HttpMock> list = new ArrayList<HttpMock>();

        httpMockRequestBean=httpmockService.query(httpMockRequestBean);
//        logger.info("返回结果数据为："+JsonUtil.object2string(httpMockRequestBean));

        return Result.wrapSuccess(httpMockRequestBean);

    }

    /*
     *注册httpmock
     * */
    @ResponseBody
    @RequestMapping(value="/add",method ={RequestMethod.POST})
    public Result add(@RequestBody String httpMockRequest){
//        logger.info("add接口请求数据为："+httpMockRequest);
        HttpMock httpmock = new HttpMock();
        try {
            httpmock =  JsonUtil.json2Object(httpMockRequest,HttpMock.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int i =httpmockService.insertHttpMock(httpmock);

        return Result.wrapSuccess(i);
    }

    /*
     *修改httpmock信息
     * */
    @ResponseBody
    @RequestMapping(value="/update",method ={RequestMethod.POST})
    public Result update(@RequestBody String httpMockRequest){
//        logger.info("update请求数据为："+httpMockRequest);
        HttpMock httpmock = new HttpMock();
        try {
            httpmock =  JsonUtil.json2Object(httpMockRequest,HttpMock.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int i =httpmockService.updateHttpMock(httpmock);

        return Result.wrapSuccess(i);
    }


    /*
     *删除httpmock信息
     * */
    @ResponseBody
    @RequestMapping(value="/delete",method ={RequestMethod.GET})
    public Result delete(@RequestParam int id){

//        logger.info("待删除待mock id为："+id);
        httpmockService.deleteHttpMock(id);

        return Result.wrapSuccess(id);
    }
}
